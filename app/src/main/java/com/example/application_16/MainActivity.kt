package com.example.application_16

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val MyPREFERENCES = "MyPrefs"
    val Name = "nameKey"
    val Email = "emailKey"
    val Mobile1 = "mobileKey_1"
    val Mobile2 = "mobileKey_2"
    val City = "cityKey"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        retriveSharedPreferences()
        val sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
        var editor= sharedpreferences.edit()
        save_btn.setOnClickListener {
            editor.putString(Name,name_edit.text.toString())
            editor.putString(Email,email_edit.text.toString())
            editor.putString(Mobile1,mobile_edit.text.toString())
            editor.putString(Mobile2,mobile_edit_2.text.toString())
            editor.putString(City,city_edit.text.toString())

        }
        kill_activity.setOnClickListener {
            editor.commit()
            finish()
        }


    }

    private fun retriveSharedPreferences() {
        val sh =getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
        name_edit.setText(sh.getString(Name,""))
        email_edit.setText(sh.getString(Email,""))
        mobile_edit.setText(sh.getString(Mobile1,""))
        mobile_edit_2.setText(sh.getString(Mobile2,"") )
        city_edit.setText(sh.getString(City,"") )
    }
}
